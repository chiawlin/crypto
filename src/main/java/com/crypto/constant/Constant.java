package com.crypto.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constant {

	public static final String BINANCE = "BINANCE";
	public static final String HOUBI = "HOUBI";
	public static final String ETHUSDT = "ETHUSDT";
	public static final String BTCUSDT = "BTCUSDT";
	
}
