package com.crypto.controller;

//import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crypto.dto.UserDTO;
import com.crypto.service.UserService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/v1")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Operation(description = "Api to retrieve the user’s crypto currencies wallet balance.", method = "GET")
	@GetMapping(value = "/user/crypto-balance/{userCode}")
	public ResponseEntity<UserDTO> getUserCryptoBalance(@PathVariable String userCode) {
		return ResponseEntity.ok().body(userService.getUserByUserCode(userCode));
	}
	

}
