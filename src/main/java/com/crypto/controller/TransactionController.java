package com.crypto.controller;

import java.time.OffsetDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crypto.dto.TransactionDTO;
import com.crypto.service.TransactionService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/v1")
public class TransactionController {

	@Autowired
	private TransactionService transactionService;

	@Operation(description = "Api to retrieve the user trading history", method = "GET")
	@GetMapping(value = "/transaction/user/{userCode}")
	public ResponseEntity<Page<TransactionDTO>> getUserTransactionHistory(@PathVariable String userCode,
			@RequestParam(defaultValue = "2022-09-01T01:30:00.000-05:00") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime fromDate,
			@RequestParam(defaultValue = "2022-09-30T01:30:00.000-05:00") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime toDate,
			@PageableDefault(size = 10, page = 0, sort = "createdDateTime", direction = Direction.DESC) Pageable pageable) {
		
		return ResponseEntity.ok().body(transactionService.listTransactionByUser(userCode, fromDate, toDate, pageable));
	}

}
