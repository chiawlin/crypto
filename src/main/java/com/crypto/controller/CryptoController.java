package com.crypto.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crypto.dto.CryptoDTO;
import com.crypto.dto.CryptoTradingRequestDTO;
import com.crypto.dto.CryptoTradingResponseDTO;
import com.crypto.service.CryptoService;
import com.crypto.service.CryptoTradingService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/v1")
public class CryptoController {
    
    @Autowired
    private CryptoService cryptoService;

    @Autowired
    private CryptoTradingService cryptoTradingService;

	@Operation(description = "Api to retrieve the latest best aggregated price.", method = "GET")
	@GetMapping(value = "/crypto/{symbol}")
	public ResponseEntity<CryptoDTO> getCryptoDetail(@PathVariable String symbol) {
		return ResponseEntity.ok().body(cryptoService.getBySymbol(symbol));
	}
	
	@Validated
	@Operation(description = "Api to trade crypto.", method = "POST")
	@PostMapping(value = "/crypto/trade")
	public ResponseEntity<CryptoTradingResponseDTO> tradeCrypto(@Valid @RequestBody CryptoTradingRequestDTO reqDto) {
		return ResponseEntity.ok().body(cryptoTradingService.tradeCrypto(reqDto));
	}

}
