package com.crypto.entity;

import java.io.Serializable;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.hibernate.annotations.ColumnDefault;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@MappedSuperclass
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class BaseEntity implements Serializable {

	private static final long serialVersionUID = -1597786740803887431L;

	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(updatable=false, nullable=false)
	@ColumnDefault("CURRENT_TIMESTAMP()")
	private OffsetDateTime createdDateTime;
	
	@Column(insertable=false)
	private OffsetDateTime updatedDateTime;
	
	@PrePersist
    private void prePersistFunction(){
		createdDateTime = OffsetDateTime.now();
    }
	
	@PreUpdate
    public void preUpdateFunction(){
		updatedDateTime = OffsetDateTime.now();
    }

}
