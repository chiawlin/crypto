package com.crypto.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@Table
@Entity
@SuperBuilder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity extends BaseEntity implements Serializable { 
	
	private static final long serialVersionUID = -5597434190656099150L;

	@Column(unique=true, nullable=false)
	private String code;
	@Column
	private String name;
	@Builder.Default
	@ColumnDefault("50000")
	@Column(nullable=false)
	private BigDecimal walletBalance = new BigDecimal(50000);
	@ColumnDefault("0")
	@Column(nullable=false)
	private BigDecimal ethereum;
	@ColumnDefault("0")
	@Column(nullable=false)
	private BigDecimal bitCoin;
	
	@PrePersist
    private void prePersist(){
		walletBalance = new BigDecimal(50000);
		ethereum = new BigDecimal(0);
		bitCoin = new BigDecimal(0);
    }

}
