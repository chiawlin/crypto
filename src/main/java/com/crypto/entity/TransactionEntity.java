package com.crypto.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.crypto.constant.OrderType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@Table
@Entity
@SuperBuilder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class TransactionEntity extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 8581156295954603192L; 
	
	@Column(nullable=false)
	private OrderType orderType;
	@Column(nullable=false)
	private BigDecimal transactionPrice;
	@Column(nullable=false)
	private BigDecimal transactionQuantity;
	
	@ManyToOne
	@JoinColumn(name = "userId", referencedColumnName = "id", 
			nullable = false, foreignKey = @ForeignKey(name = "fk_user_id"))
	private UserEntity user;
	
	@ManyToOne
	@JoinColumn(name="cryptoId", referencedColumnName = "id", 
			nullable = false, foreignKey = @ForeignKey(name = "fk_crypto_id"))
	private CryptoEntity crypto;
	
	
}
