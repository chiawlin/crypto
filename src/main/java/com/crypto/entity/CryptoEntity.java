package com.crypto.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@Table
@Entity
@SuperBuilder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class CryptoEntity extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -8164718484738579493L;

	@Column(unique=true)
	private String symbol;
	@Column
	private BigDecimal sellPrice;
	@Column
	private BigDecimal buyPrice;
	@Column
	private BigDecimal sellQuantity;
	@Column
	private BigDecimal buyQuantity;
	@Column
	private String sourceOfBuying;
	@Column
	private String sourceOfSelling;

}
