package com.crypto.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.crypto.constant.OrderType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CryptoTradingResponseDTO implements Serializable {
 
	private static final long serialVersionUID = 4420602631774336858L;
	
	private String userCode;
	private OrderType orderType;
	private BigDecimal walletBalance;
	private String symbol;
	private Integer unit;
	private BigDecimal unitPrice;
	private BigDecimal totalPrice;

}
