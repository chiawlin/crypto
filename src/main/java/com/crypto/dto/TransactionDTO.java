package com.crypto.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.crypto.constant.OrderType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class TransactionDTO extends BaseDTO implements Serializable {

	private static final long serialVersionUID = -6773089584635195777L;
	
	private OrderType orderType;
	private BigDecimal transactionPrice;
	private BigDecimal transactionQuantity;
	private UserDTO userDto;
	private CryptoDTO cryptoDto;
	
}
