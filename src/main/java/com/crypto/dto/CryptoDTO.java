package com.crypto.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class CryptoDTO extends BaseDTO implements Serializable {
 
	private static final long serialVersionUID = -3611880433507489564L;
	
	private String symbol;
	private BigDecimal sellPrice;
	private BigDecimal buyPrice;
	private BigDecimal sellQuantity;
	private BigDecimal buyQuantity;
	private String sourceOfBuying;
	private String sourceOfSelling;

}
