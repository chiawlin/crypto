package com.crypto.dto;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.crypto.constant.OrderType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CryptoTradingRequestDTO implements Serializable {

	private static final long serialVersionUID = -1056033812514837487L;
	
	@NotNull(message="userCode must not be null")
	private String userCode;
	@NotNull(message="orderType must not be null")
	private OrderType orderType;
	@NotNull(message="symbol must not be null")
	private String symbol;
	@NotNull
	@Min(message="unit must be greater than 0", value = 1)
	private Integer unit;

}
