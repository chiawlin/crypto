package com.crypto.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HuobiResponseDTO implements Serializable {

	private static final long serialVersionUID = 4820193904369695788L;

	private List<HuobiResponseDataDTO> data;
	
	
}
