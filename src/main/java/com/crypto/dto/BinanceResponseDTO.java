package com.crypto.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BinanceResponseDTO implements Serializable {

	private static final long serialVersionUID = -8720354079667721965L;
	
	private String symbol;
	private String bidPrice;
	private String askPrice;
	private String bidQty;
	private String askQty;
	
	
}
