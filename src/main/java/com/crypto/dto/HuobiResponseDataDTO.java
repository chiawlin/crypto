package com.crypto.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HuobiResponseDataDTO implements Serializable {

	private static final long serialVersionUID = 1194364870375307811L;

	private String symbol;
	private BigDecimal bid;
	private BigDecimal ask;
	private BigDecimal bidSize;
	private BigDecimal askSize;
	
	
}
