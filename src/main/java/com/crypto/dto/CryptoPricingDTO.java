package com.crypto.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import com.crypto.constant.Constant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CryptoPricingDTO implements Serializable {
 
	private static final long serialVersionUID = -2910912180472908434L;
	
	private String symbol;
	private BigDecimal sellPrice;
	private BigDecimal buyPrice;
	private BigDecimal sellQuantity;
	private BigDecimal buyQuantity;
	private String sourceOfSelling;
	private String sourceOfBuying;
	private String source;
	
	public CryptoPricingDTO getCryptoPricingDTO(BinanceResponseDTO source) {
		return CryptoPricingDTO.builder()
				.symbol(source.getSymbol())
				.sellPrice(!Objects.isNull(source.getBidPrice()) ? new BigDecimal(source.getBidPrice()) : new BigDecimal(0))
				.sellQuantity(!Objects.isNull(source.getBidQty()) ? new BigDecimal(source.getBidQty()) : new BigDecimal(0))
				.buyPrice(!Objects.isNull(source.getAskPrice()) ? new BigDecimal(source.getAskPrice()) : new BigDecimal(0))
				.buyQuantity(!Objects.isNull(source.getAskQty()) ? new BigDecimal(source.getAskQty()) : new BigDecimal(0))
				.build();
	}
	
	public CryptoPricingDTO(HuobiResponseDataDTO source) {
		this.setSymbol(source.getSymbol());
		this.setSellPrice(source.getBid());
		this.setSellQuantity(source.getBidSize());
		this.setBuyPrice(source.getAsk());
		this.setBuyQuantity(source.getAskSize());
		this.setSource(Constant.HOUBI);
	}

	public CryptoPricingDTO(BinanceResponseDTO source) {
		this.setSymbol(source.getSymbol());
		this.setSellPrice(!Objects.isNull(source.getBidPrice()) ? new BigDecimal(source.getBidPrice()) : new BigDecimal(0));
		this.setSellQuantity(!Objects.isNull(source.getBidQty()) ? new BigDecimal(source.getBidQty()) : new BigDecimal(0));
		this.setBuyPrice(!Objects.isNull(source.getAskPrice()) ? new BigDecimal(source.getAskPrice()) : new BigDecimal(0));
		this.setBuyQuantity(!Objects.isNull(source.getAskQty()) ? new BigDecimal(source.getAskQty()) : new BigDecimal(0));
		this.setSource(Constant.BINANCE);
	}
	
}
