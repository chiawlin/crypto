package com.crypto.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO extends BaseDTO implements Serializable {

	private static final long serialVersionUID = 3541745025950348285L;
	
	private String code;
	private String name;
	private BigDecimal walletBalance;
	private BigDecimal ethereum;
	private BigDecimal bitCoin;
}
