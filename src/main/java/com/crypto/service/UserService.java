package com.crypto.service;

import java.util.Objects;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crypto.dto.UserDTO;
import com.crypto.entity.UserEntity;
import com.crypto.repository.UserRepository;

@Service
@Transactional
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public UserDTO saveUser(UserDTO dto) {
		UserEntity entity = modelMapper.map(dto, UserEntity.class);
		return modelMapper.map(userRepository.save(entity), UserDTO.class);
	}
	
	public UserDTO getUserByCode(String code) {
		Optional<UserEntity> userEntity = userRepository.findByCode(code);
		if (userEntity.isPresent())
			return modelMapper.map(userEntity.get(), UserDTO.class);
		else 
			return null;
		
	}

	public UserDTO getUserByUserCode(String code) {
		UserDTO dto = getUserByCode(code);
		if(Objects.isNull(dto)) {
			throw new RuntimeException("User not found");
		}
		return dto;
	}
}
