package com.crypto.service.integration;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.crypto.dto.BinanceResponseDTO;
import com.crypto.dto.HuobiResponseDTO;
import com.crypto.dto.HuobiResponseDataDTO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PriceSourceService {

	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${com.crypto.integration.binance.url:https://api.binance.com/api/v3/ticker/bookTicker}")
    private String binanceUrl;
	
	@Value("${com.crypto.integration.huobi.url:https://api.huobi.pro/market/tickers}")
    private String huobiUrl;

	public List<BinanceResponseDTO> getBinancePricing() {
		List<BinanceResponseDTO> res = new ArrayList<>();
		try {
			ResponseEntity<List<BinanceResponseDTO>> responseEntity = restTemplate.exchange(
					binanceUrl, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<BinanceResponseDTO>>() {
					});
			res = responseEntity.getBody();
		} catch (RuntimeException ex) {
			log.error("Error encountered when calling Binance data source: ", ex);
		}
		return res;
	}

	public List<HuobiResponseDataDTO> getHuobiPricing() {
		List<HuobiResponseDataDTO> res = new ArrayList<>();
		try {
			ResponseEntity<HuobiResponseDTO> responseEntity = restTemplate
					.getForEntity(huobiUrl, HuobiResponseDTO.class);
			if (!Objects.isNull(responseEntity.getBody())) {
				res = responseEntity.getBody().getData();
			}
		} catch (RuntimeException ex) {
			log.error("Error encountered when calling Binance data source", ex);
		}
		return res;
	}
}
