package com.crypto.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crypto.dto.CryptoDTO;
import com.crypto.entity.CryptoEntity;
import com.crypto.repository.CryptoRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class CryptoService {
	
	@Autowired
	private CryptoRepository cryptoRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public CryptoDTO getBySymbol(String symbol) {
		try {
			CryptoDTO dto = modelMapper.map(cryptoRepository.findBySymbol(symbol).orElseThrow(), CryptoDTO.class);
			return dto;
		} catch (RuntimeException ex) {
			log.error("failed to call getBySymbol --> ", ex);
			throw new NoSuchElementException("No record found");
		}
		
	}
		
	public List<CryptoEntity> listBySymbols(List<String> symbolList) {
		return cryptoRepository.findAllBySymbolIn(symbolList);
	}
	
	public void saveCryptos(List<CryptoDTO> dtos) {
		List<CryptoEntity> entities = dtos.stream().map(dto -> modelMapper.map(dto, CryptoEntity.class))
				.collect(Collectors.toList());
		cryptoRepository.saveAll(entities);
	}
	
}
