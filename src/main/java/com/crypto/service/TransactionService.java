package com.crypto.service;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

import javax.validation.ValidationException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crypto.converter.TransactionEntityConverter;
import com.crypto.dto.TransactionDTO;
import com.crypto.entity.TransactionEntity;
import com.crypto.repository.TransactionRepository;

@Service
@Transactional
public class TransactionService {
	
	@Autowired
	private TransactionRepository transactionRepository;
	
	@Autowired
	private TransactionEntityConverter transactionEntityConverter;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Value("${com.crypto.transaction-listing-max-day-range:90}")
	private int dayRange;
	
	public TransactionDTO saveTransaction(TransactionDTO dto) {
		TransactionEntity entity = modelMapper.map(dto, TransactionEntity.class);
		return modelMapper.map(transactionRepository.save(entity), TransactionDTO.class);
	}
	
	public Page<TransactionDTO> listTransactionByUser(String userCode, OffsetDateTime fromDate, OffsetDateTime toDate,
			Pageable pageable) {
		validateListTransactionByUserRequest(userCode, fromDate, toDate);
		
		Page<TransactionEntity> entityPageObj = transactionRepository.findByUserCodeAndCreatedDateTimeBetween(userCode, fromDate, toDate, pageable);
		
		return entityPageObj.map(transactionEntityConverter::convert);
	}
	
	private void validateListTransactionByUserRequest(String userCode, OffsetDateTime fromDate, OffsetDateTime toDate) {
		if (Objects.isNull(userCode))
			throw new ValidationException("userCode must not be null");
		if (Objects.isNull(fromDate))
			throw new ValidationException("fromDate must not be null");
		if (Objects.isNull(toDate))
			throw new ValidationException("toDate must not be null");
		if (toDate.isBefore(fromDate))
			throw new ValidationException("toDate must not after fromDate");
		
		long reqDayRange = ChronoUnit.DAYS.between(fromDate, toDate);
		if (reqDayRange > dayRange)
			throw new ValidationException("requested date range exceeds limit");
	}
	
}
