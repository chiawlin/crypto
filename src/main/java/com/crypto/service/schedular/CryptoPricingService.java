package com.crypto.service.schedular;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.crypto.dto.BinanceResponseDTO;
import com.crypto.dto.CryptoDTO;
import com.crypto.dto.CryptoPricingDTO;
import com.crypto.dto.HuobiResponseDataDTO;
import com.crypto.entity.CryptoEntity;
import com.crypto.service.CryptoService;
import com.crypto.service.integration.PriceSourceService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class CryptoPricingService {
	
	@Autowired
	private PriceSourceService priceSourceService;
	
	@Autowired
	private CryptoService cryptoService;

	@Autowired
	private ModelMapper modelMapper;
	
	@Value("${com.crypto.supported.symbol:ETHUSDT,BTCUSDT}")
	private String[] supportedSymbol;
	
	@Scheduled(cron = "${com.crypto.pricing.scheduler-cron:*/10 * * * * *}")
    public void perform() {
		log.debug("scheduler job started at: " + LocalDateTime.now());
		updateCryptosFromSource();
    }
	
	public void updateCryptosFromSource() {
		List<CryptoPricingDTO> cryptoPricingDtoList = getAggregatePricing();
		
		List<String> symbolList = new ArrayList<>();
		List<CryptoDTO> latestCryptoDtoList = new ArrayList<>();
		for (CryptoPricingDTO dto : cryptoPricingDtoList) {
			symbolList.add(dto.getSymbol()); 
			latestCryptoDtoList.add(modelMapper.map(dto, CryptoDTO.class));
		}
		List<CryptoEntity> cryptoEntityList = cryptoService.listBySymbols(symbolList);
		latestCryptoDtoList.forEach(dto -> 
			cryptoEntityList.forEach(entity -> {
				if(StringUtils.equalsIgnoreCase(entity.getSymbol(), dto.getSymbol()))
					dto.setId(entity.getId());
			})
		);
		
		log.debug("latestCryptoDtoList --> ", latestCryptoDtoList);
		cryptoService.saveCryptos(latestCryptoDtoList);
	}
	
	private List<CryptoPricingDTO> getAggregatePricing() {
		try {
			if (!CollectionUtils.isEmpty(Arrays.asList(supportedSymbol))) {
				List<CryptoPricingDTO> cryptoPricingList = new ArrayList<>();
								
				CompletableFuture<List<CryptoPricingDTO>> getBinancePricing = CompletableFuture.supplyAsync(() -> {
					List<CryptoPricingDTO> temp = new ArrayList<>();
					List<BinanceResponseDTO> binanceResp = priceSourceService.getBinancePricing();
					Set<BinanceResponseDTO> filteredBinanceResp = binanceResp.parallelStream()
							.filter(item -> Arrays.asList(supportedSymbol).contains(StringUtils.upperCase(item.getSymbol()))).collect(Collectors.toSet());
					log.debug("filteredBinanceResp ---> " + filteredBinanceResp);
					if(!CollectionUtils.isEmpty(filteredBinanceResp)) {
						temp.addAll(filteredBinanceResp.stream().map(CryptoPricingDTO::new).collect(Collectors.toList()));
					}
					return temp;
				});	

				CompletableFuture<List<CryptoPricingDTO>> getHuobiPricing = CompletableFuture.supplyAsync(() -> {
					List<CryptoPricingDTO> temp = new ArrayList<>();
					List<HuobiResponseDataDTO> huobiResp = priceSourceService.getHuobiPricing();
					Set<HuobiResponseDataDTO> filteredHuobiResp = huobiResp.parallelStream()
							.filter(item -> Arrays.asList(supportedSymbol).contains(StringUtils.upperCase(item.getSymbol()))).collect(Collectors.toSet());
					log.debug("filteredHoubiResp ---> " + filteredHuobiResp);
					if(!CollectionUtils.isEmpty(filteredHuobiResp)) {
						temp.addAll(filteredHuobiResp.stream().map(CryptoPricingDTO::new).collect(Collectors.toList()));
					}
					return temp;
				});	

				CompletableFuture.allOf(getBinancePricing, getHuobiPricing).join();
				
				cryptoPricingList.addAll(getBinancePricing.get());
				cryptoPricingList.addAll(getHuobiPricing.get());
				
				if (!CollectionUtils.isEmpty(cryptoPricingList)) {			
					return comparePricing(cryptoPricingList);
				}
			}
			return Collections.emptyList();
		} catch (InterruptedException | ExecutionException e ) {  // NOSONAR
			throw new RuntimeException("failed to getAggregatePricing -> ", e);
		} 
	}
	
	private List<CryptoPricingDTO> comparePricing(List<CryptoPricingDTO> source) {

		Map<String, CryptoPricingDTO> map = new HashMap<>();

		Arrays.asList(supportedSymbol).forEach(symbol -> {
			CryptoPricingDTO buyPricingDto = source.stream()
					.filter(item -> StringUtils.equalsIgnoreCase(item.getSymbol(), symbol)).collect(Collectors.toList())
					.stream().min(Comparator.comparing(CryptoPricingDTO::getBuyPrice))
					.orElseThrow(NoSuchElementException::new);
			map.put(symbol, CryptoPricingDTO.builder().symbol(symbol).buyPrice(buyPricingDto.getBuyPrice())
					.buyQuantity(buyPricingDto.getBuyQuantity()).sourceOfBuying(buyPricingDto.getSource()).build());
		});

		map.entrySet().forEach(entry -> {
			CryptoPricingDTO sellPricingDto = source.stream()
					.filter(item -> StringUtils.equalsIgnoreCase(item.getSymbol(), entry.getKey()))
					.collect(Collectors.toList()).stream().max(Comparator.comparing(CryptoPricingDTO::getSellPrice))
					.orElseThrow(NoSuchElementException::new);
			CryptoPricingDTO value = entry.getValue();
			value.setSellPrice(sellPricingDto.getSellPrice());
			value.setSellQuantity(sellPricingDto.getSellQuantity());
			value.setSourceOfSelling(sellPricingDto.getSource());
			entry.setValue(value);
		});

		return new ArrayList<>(map.values());
	}


}
