package com.crypto.service;

import java.math.BigDecimal;
import java.util.NoSuchElementException;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crypto.constant.Constant;
import com.crypto.constant.OrderType;
import com.crypto.dto.CryptoDTO;
import com.crypto.dto.CryptoTradingRequestDTO;
import com.crypto.dto.CryptoTradingResponseDTO;
import com.crypto.dto.TransactionDTO;
import com.crypto.dto.UserDTO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class CryptoTradingService {

	@Autowired
	private CryptoService cryptoService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TransactionService trxnService;
	
	public CryptoTradingResponseDTO tradeCrypto(CryptoTradingRequestDTO reqDto) {
		validateTradeCryptoRequest(reqDto);
		
		UserDTO userDto = userService.getUserByCode(reqDto.getUserCode());
		if (Objects.isNull(userDto)){
			log.debug("no existing user found, proceed to create new user for trading");
			userDto = userService
					.saveUser(UserDTO.builder().code(reqDto.getUserCode()).name(reqDto.getUserCode()).build());
		}
		
		CryptoDTO cryptoDto = cryptoService.getBySymbol(reqDto.getSymbol());
		if (Objects.isNull(cryptoDto)) {
			log.error("symbol not found");
			throw new NoSuchElementException("Invalid symbol --> " + reqDto.getSymbol());
		}
		
		validateUserCryptoAssetAndBalance(reqDto, userDto, cryptoDto);
		
		TransactionDTO trxnDto = constructTransactionDTO(reqDto, userDto, cryptoDto);
		updateUserCryptoAssetAndBalance(trxnDto, userDto);
		trxnService.saveTransaction(trxnDto);
		
		return CryptoTradingResponseDTO.builder()
				.userCode(userDto.getCode())
				.orderType(trxnDto.getOrderType())
				.walletBalance(userDto.getWalletBalance())
				.symbol(cryptoDto.getSymbol())
				.unit(reqDto.getUnit())
				.unitPrice(OrderType.BUY.equals(trxnDto.getOrderType()) ? cryptoDto.getBuyPrice()
						: cryptoDto.getSellPrice())
				.totalPrice(BigDecimal.valueOf(reqDto.getUnit())
						.multiply(OrderType.BUY.equals(trxnDto.getOrderType()) ? cryptoDto.getBuyPrice()
								: cryptoDto.getSellPrice()))
				.build();
	}
	
	private void updateUserCryptoAssetAndBalance(TransactionDTO trxnDto, UserDTO userDto) {
		if (OrderType.BUY.equals(trxnDto.getOrderType())) {
			if (Constant.BTCUSDT.equalsIgnoreCase(trxnDto.getCryptoDto().getSymbol())) {
				userDto.setWalletBalance(userDto.getWalletBalance().subtract(trxnDto.getTransactionPrice()));
				userDto.setBitCoin(userDto.getBitCoin().add(trxnDto.getTransactionPrice()));
			}
			else if (Constant.ETHUSDT.equalsIgnoreCase(trxnDto.getCryptoDto().getSymbol())) {
				userDto.setWalletBalance(userDto.getWalletBalance().subtract(trxnDto.getTransactionPrice()));
				userDto.setEthereum(userDto.getEthereum().add(trxnDto.getTransactionPrice()));
			}
		}
		
		else if (OrderType.SELL.equals(trxnDto.getOrderType())) {
			if (Constant.BTCUSDT.equalsIgnoreCase(trxnDto.getCryptoDto().getSymbol())) {
				userDto.setWalletBalance(userDto.getWalletBalance().add(trxnDto.getTransactionPrice()));
				userDto.setBitCoin(userDto.getBitCoin().subtract(trxnDto.getTransactionPrice()));
			}
			else if (Constant.ETHUSDT.equalsIgnoreCase(trxnDto.getCryptoDto().getSymbol())) {
				userDto.setWalletBalance(userDto.getWalletBalance().add(trxnDto.getTransactionPrice()));
				userDto.setEthereum(userDto.getEthereum().subtract(trxnDto.getTransactionPrice()));
			}
		}	
		userService.saveUser(userDto);
	}
	
	private TransactionDTO constructTransactionDTO(CryptoTradingRequestDTO reqDto, UserDTO userDto, CryptoDTO cryptoDto) {
		return TransactionDTO.builder()
				.orderType(reqDto.getOrderType())
				.transactionPrice(cryptoDto.getBuyPrice().multiply(BigDecimal.valueOf(reqDto.getUnit())))
				.transactionQuantity(BigDecimal.valueOf(reqDto.getUnit()))
				.userDto(userDto)
				.cryptoDto(cryptoDto)
				.build();
	}
	
	private void validateUserCryptoAssetAndBalance(CryptoTradingRequestDTO reqDto, UserDTO userDto, CryptoDTO cryptoDto) {
		if (OrderType.BUY.equals(reqDto.getOrderType())) {
			BigDecimal totalPrice = cryptoDto.getBuyPrice().multiply(BigDecimal.valueOf(reqDto.getUnit()));
			if (totalPrice.compareTo(userDto.getWalletBalance()) > 0)
				throw new RuntimeException("User do not have enough balance");
		}
		
		if (OrderType.SELL.equals(reqDto.getOrderType())) {
			BigDecimal totalCrypto = cryptoDto.getSellPrice().multiply(BigDecimal.valueOf(reqDto.getUnit()));
			if (Constant.BTCUSDT.equalsIgnoreCase(reqDto.getSymbol()) && totalCrypto.compareTo(userDto.getBitCoin()) > 0)
				throw new RuntimeException("User do not have enough crypto " + reqDto.getSymbol());
			if (Constant.ETHUSDT.equalsIgnoreCase(reqDto.getSymbol()) && totalCrypto.compareTo(userDto.getEthereum()) > 0)
				throw new RuntimeException("User do not have enough crypto " + reqDto.getSymbol());
		}
	}
	
	private void validateTradeCryptoRequest(CryptoTradingRequestDTO reqDto) {  //NOSONAR
		// TODO
	}

}
