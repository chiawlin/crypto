package com.crypto.converter;

import java.util.Objects;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.crypto.dto.CryptoDTO;
import com.crypto.dto.TransactionDTO;
import com.crypto.dto.UserDTO;
import com.crypto.entity.TransactionEntity;


@Component
public class TransactionEntityConverter implements Converter<TransactionEntity, TransactionDTO> {

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public TransactionDTO convert(TransactionEntity source) {
		TransactionDTO dto = modelMapper.map(source, TransactionDTO.class);
		if (!Objects.isNull(source.getUser())) {
			UserDTO userDto = modelMapper.map(source.getUser(), UserDTO.class);
			dto.setUserDto(userDto);
		}
		if (!Objects.isNull(source.getCrypto())) {
			CryptoDTO cryptoDto = modelMapper.map(source.getCrypto(), CryptoDTO.class);
			dto.setCryptoDto(cryptoDto);
		}
		return dto;
	}

}
