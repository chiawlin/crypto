package com.crypto.repository;

import java.time.OffsetDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crypto.entity.TransactionEntity;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity, Long> {
	
	Page<TransactionEntity> findByUserCodeAndCreatedDateTimeBetween(String userCode, OffsetDateTime fromDate, OffsetDateTime toDate, Pageable pageable);
		
}
