package com.crypto.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crypto.entity.CryptoEntity;

@Repository
public interface CryptoRepository extends JpaRepository<CryptoEntity, Long> {

	List<CryptoEntity> findAllBySymbolIn(List<String> symbols);
	
	Optional<CryptoEntity> findBySymbol(String symbol);
}
